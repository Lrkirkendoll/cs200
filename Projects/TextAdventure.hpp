#ifndef _TEXT_ADVENTURE_HPP
#define _TEXT_ADVENTURE_HPP

#include "Room.hpp"

class TextAdventure
{
    public:
    TextAdventure();
    ~TextAdventure();
    void Run();


    private:
    bool m_isDone;
    Room* m_rooms; //dynamic array
    int m_totalRooms;
    Room* m_ptrCurrentRoom; // pointer

    //Memory management
    void AllocateSpace(int size);
    void DeallocateSpace();

    //File loading
    void LoadGameData();
    int GetIndexOfRoomWithName( string name);

    //Game loop functions
    string GetUserCommand();
    void TryToMove( string command );



};





#endif // _TEXT_ADVENTURE_HPP
